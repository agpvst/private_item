<?php

namespace Drupal\private_item\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\private_item\PrivateItemTypeInterface;

/**
 * Defines the private item type entity.
 *
 * @ConfigEntityType(
 *   id = "private_item_type",
 *   label = @Translation("Private item type"),
 *   handlers = {
 *     "storage" = "Drupal\private_item\PrivateItemTypeStorage",
 *     "list_builder" = "Drupal\private_item\PrivateItemTypeListBuilder",
 *     "form" = {
 *       "default" = "Drupal\private_item\PrivateItemTypeForm",
 *       "delete" = "Drupal\private_item\Form\PrivateItemTypeDeleteForm"
 *     }
 *   },
 *   admin_permission = "administer private item types",
 *   config_prefix = "type",
 *   bundle_of = "private_item",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "delete-form" = "/admin/structure/private-item-types/manage/{private_item_type}/delete",
 *     "edit-form" = "/admin/structure/private-item-types/manage/{private_item_type}",
 *     "collection" = "/admin/structure/private-item-types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   }
 * )
 */
class PrivateItemType extends ConfigEntityBundleBase implements PrivateItemTypeInterface {

  /**
   * The private item type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * Label of the private item type.
   *
   * @var string
   */
  protected $label;

  /**
   * Description of the private item type.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // Reset caches.
    $storage->resetCache(array_keys($entities));
  }

}
