<?php

namespace Drupal\private_item\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\private_item\PrivateItemInterface;
use Drupal\user\UserInterface;

/**
 * Defines the private item entity.
 *
 * @ContentEntityType(
 *   id = "private_item",
 *   label = @Translation("Private item"),
 *   bundle_label = @Translation("Private item type"),
 *   handlers = {
 *     "view_builder" = "Drupal\private_item\PrivateItemViewBuilder",
 *     "access" = "Drupal\private_item\PrivateItemAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "form" = {
 *       "default" = "Drupal\private_item\PrivateItemForm",
 *       "edit" = "Drupal\private_item\PrivateItemForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *   },
 *   admin_permission = "administer private items",
 *   base_table = "private_item",
 *   data_table = "private_item_field_data",
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "label",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid"
 *   },
 *   bundle_entity_type = "private_item_type",
 *   field_ui_base_route = "entity.private_item_type.edit_form",
 *   common_reference_target = TRUE,
 *   links = {
 *     "canonical" = "/item/{private_item}",
 *     "delete-form" = "/item/{private_item}/delete",
 *     "edit-form" = "/item/{private_item}/edit",
 *   },
 *   permission_granularity = "bundle"
 * )
 */
class PrivateItem extends ContentEntityBase implements PrivateItemInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner'))
      ->setDescription(t('The owner of the item.'))
      ->setSetting('target_type', 'user')
      ->setReadOnly(TRUE);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setDescription(t('The item label.'))
      ->setTranslatable(TRUE)
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', array(
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -5,
      ))
      ->setDisplayConfigurable('form', TRUE);

    $fields['shared'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Shared'))
      ->setDescription(t('Whether the item is shared.'))
      ->setTranslatable(TRUE)
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the item was created.'))
      ->setTranslatable(TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the item was last edited.'))
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * Returns the entity owner's user entity.
   *
   * @return \Drupal\user\UserInterface
   *   The owner user entity.
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * Sets the entity owner's user entity.
   *
   * @param \Drupal\user\UserInterface $account
   *   The owner user entity.
   *
   * @return $this
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * Returns the entity owner's user ID.
   *
   * @return int|null
   *   The owner user ID, or NULL in case the user ID field has not been set on
   *   the entity.
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * Sets the entity owner's user ID.
   *
   * @param int $uid
   *   The owner user id.
   *
   * @return $this
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }


    /**
     * Returns item's "shared" status.
     *
     * @return bool
     *   TRUE if private item is shared, FALSE otherwise.
     */
    public function isShared() {
        return $this->get('shared')->value;
    }

}
