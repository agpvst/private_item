<?php

namespace Drupal\private_item;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;

/**
 * Defines the private item schema handler.
 */
class PrivateItemStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset = FALSE);

    $schema['private_item_field_data']['indexes'] += [
      'private_item__type_uid_created' => ['type', 'uid', 'created'],
      'private_item__type_created' => ['type', 'created'],
      'private_item__created' => ['created'],
    ];

    return $schema;
  }

}
