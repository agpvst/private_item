<?php

namespace Drupal\private_item;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\language\Entity\ContentLanguageSettings;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form for private item type edit forms.
 */
class PrivateItemTypeForm extends BundleEntityFormBase {

  /**
   * The private item type storage.
   *
   * @var \Drupal\private_item\PrivateItemTypeStorageInterface.
   */
  protected $itemTypeStorage;

  /**
   * Constructs a new private item type form.
   *
   * @param \Drupal\private_item\PrivateItemTypeStorageInterface $itemTypeStorage
   *   The private item type storage.
   */
  public function __construct(PrivateItemTypeStorageInterface $itemTypeStorage) {
    $this->itemTypeStorage = $itemTypeStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')->getStorage('private_item_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var PrivateItemTypeInterface $item_type */
    $item_type = $this->entity;
    if ($item_type->isNew()) {
      $form['#title'] = $this->t('Add item type');
    }
    else {
      $form['#title'] = $this->t('Edit %label item type', ['%label' => $item_type->label()]);
    }

    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $item_type->label(),
      '#maxlength' => 255,
      '#required' => TRUE,
    );
    $form['id'] = array(
      '#type' => 'machine_name',
      '#default_value' => $item_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => array(
        'exists' => array($this, 'exists'),
        'source' => array('label'),
      ),
    );
    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#default_value' => $item_type->getDescription(),
    );

    if ($this->moduleHandler->moduleExists('language')) {
      $form['language'] = array(
        '#type' => 'details',
        '#title' => t('Language settings'),
        '#group' => 'additional_settings',
      );

      $language_configuration = ContentLanguageSettings::loadByEntityTypeBundle('private_item', $item_type->id());
      $form['language']['language_configuration'] = array(
        '#type' => 'language_configuration',
        '#entity_information' => array(
          'entity_type' => 'private_item',
          'bundle' => $item_type->id(),
        ),
        '#default_value' => $language_configuration,
      );
    }

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var PrivateItemTypeInterface $item_type */
    $item_type = $this->entity;

    // Prevent leading and trailing spaces in item type names.
    $item_type->set('label', trim($item_type->label()));

    $status = $item_type->save();
    $edit_link = $item_type->toLink($this->t('Edit'), 'edit-form')->toString();
    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created new private item type %label.', array('%label' => $item_type->label())));
        $this->logger('private_item')->notice('Created new private item type %label.', array('%label' => $item_type->label(), 'link' => $edit_link));
        break;

      case SAVED_UPDATED:
        drupal_set_message($this->t('Updated private item type %label.', array('%label' => $item_type->label())));
        $this->logger('private_item')->notice('Updated private item type %label.', array('%label' => $item_type->label(), 'link' => $edit_link));
        break;
    }

    $form_state->setRedirectUrl($item_type->toUrl('collection'));
  }

  /**
   * Determines if the private item type already exists.
   *
   * @param string $itemTypeId
   *   The private item type ID.
   *
   * @return bool
   *   TRUE if the type exists, FALSE otherwise.
   */
  public function exists($itemTypeId) {
    $itemType = $this->itemTypeStorage->load($itemTypeId);
    return !empty($itemType);
  }

}
