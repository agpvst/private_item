<?php

namespace Drupal\private_item;

use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Database\Connection;

/**
 * Defines a storage handler class for private item types.
 */
class PrivateItemTypeStorage extends ConfigEntityStorage implements PrivateItemTypeStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function getItemCount($type_id) {
    /** @var Connection $database */
    $database = \Drupal::getContainer()->get('database');
    return $database
      ->query('SELECT COUNT(li.id) FROM {private_item} li WHERE li.type = :type_id', [':type_id' => $type_id])
      ->fetchField();
  }
}
