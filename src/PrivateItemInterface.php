<?php

namespace Drupal\private_item;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a private item entity.
 */
interface PrivateItemInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

    /**
     * Returns item's "shared" status.
     *
     * @return bool
     *   TRUE if private item is shared, FALSE otherwise.
     */
    public function isShared();

}
