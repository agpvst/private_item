<?php

namespace Drupal\private_item;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\private_item\Entity\PrivateItem;

/**
 * View builder handler for private items.
 */
class PrivateItemViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function alterBuild(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
    /** @var PrivateItem $entity */
    parent::alterBuild($build, $entity, $display, $view_mode);
    $build['#contextual_links']['private_item'] = array(
      'route_parameters' => array('private_item' => $entity->id()),
      'metadata' => array('changed' => $entity->getChangedTime()),
    );
  }

}
