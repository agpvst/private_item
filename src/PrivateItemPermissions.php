<?php

namespace Drupal\private_item;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\private_item\Entity\PrivateItemType;

/**
 * Provides dynamic permissions for private items of different types.
 */
class PrivateItemPermissions {

  use StringTranslationTrait;

  /**
   * Returns an array of private item type permissions.
   *
   * @return array
   *   The private item type permissions.
   */
  public function itemTypePermissions() {
    $perms = array();
    // Generate permissions for all private item types.
    foreach (PrivateItemType::loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }

    return $perms;
  }

  /**
   * Returns permissions for a given private item type.
   *
   * @param \Drupal\private_item\Entity\PrivateItemType $type
   *   The private item type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(PrivateItemType $type) {
    $type_id = $type->id();
    $type_params = ['%type_name' => $type->label()];

    $ops = [
      'view' => 'View',
      'create' => 'Create',
      'update' => 'Update',
      'delete' => 'Delete',
    ];

    $permissions = [];

    foreach ($ops as $op_name => $op_title) {
      $permissions["$op_name $type_id private items"] = [
        'title' => $this->t("%type_name: $op_title private items", $type_params),
      ];
    }

    return $permissions;
  }

}
