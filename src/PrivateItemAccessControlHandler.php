<?php

namespace Drupal\private_item;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the private item entity type.
 *
 * @see \Drupal\private_item\Entity\PrivateItem
 */
class PrivateItemAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\private_item\Entity\PrivateItem $entity */

    // Administrators have unrestricted access to private items.
    if ($account->hasPermission('administer private items')) {
      return AccessResult::allowed()->cachePerUser();
    }

    // Users with appropriate permission can view shared items.
    if ($operation == 'view' && $entity->isShared()) {
      return AccessResult::allowedIfHasPermission($account, 'view shared private items')
        ->addCacheableDependency($entity);
    }

    $uid = $entity->getOwnerId();
    $aid = $account->id();
    $type = $entity->bundle();

    // By default, only item author with proper permission have access to it.
    return AccessResult::allowedIf(($aid == $uid) && $account->hasPermission("$operation $type private items"))
      ->cachePerUser()
      ->addCacheableDependency($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, [
      'administer private items',
      "create $entity_bundle private items",
    ], 'OR');
  }

  /**
   * @inheritDoc
   */
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    // Only administrators can create shared items.
    if ($operation == 'edit' && $field_definition->getName() == 'shared') {
      return AccessResult::allowedIfHasPermission($account, 'administer private items');
    }

    // Fallback to parent implementation.
    return parent::checkFieldAccess($operation, $field_definition, $account, $items);
  }

}
