<?php

namespace Drupal\private_item;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;

/**
 * Defines an interface for private item type entity storage classes.
 */
interface PrivateItemTypeStorageInterface extends ConfigEntityStorageInterface {

  /**
   * Gets count of private items of this type.
   *
   * @param string $type_id
   *   Private item type ID.
   *
   * @return int
   *   Count of items of this type.
   */
  public function getItemCount($type_id);

}
