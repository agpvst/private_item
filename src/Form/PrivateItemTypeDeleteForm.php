<?php

namespace Drupal\private_item\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\private_item\PrivateItemTypeStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a deletion confirmation form for private item type.
 */
class PrivateItemTypeDeleteForm extends EntityDeleteForm {

  /**
   * The private item type storage object.
   *
   * @var PrivateItemTypeStorageInterface
   */
  protected $itemTypeStorage;

  /**
   * Constructs a new PrivateItemTypeDeleteForm object.
   *
   * @param PrivateItemTypeStorageInterface $itemTypeStorage
   *   The private item type storage object.
   */
  public function __construct(PrivateItemTypeStorageInterface $itemTypeStorage) {
    $this->itemTypeStorage = $itemTypeStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('private_item_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $item_type = $this->entity;
    $num_items = $this->itemTypeStorage->getItemCount($item_type->id());
    if ($num_items) {
      $caption = '<p>' . $this->formatPlural($num_items,
          'There is 1 item of type %type on the site. You can not remove this type until you have removed all of the %type items.',
          'There are @count items of type %type on the site. You can not remove this type until you have removed all of the %type items.',
          ['%type' => $item_type->label()]) . '</p>';
      $form['#title'] = $this->getQuestion();
      $form['description'] = array('#markup' => $caption);
      return $form;
    }

    return parent::buildForm($form, $form_state);
  }

}
