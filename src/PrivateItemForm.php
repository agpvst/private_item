<?php

namespace Drupal\private_item;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base handler for private item edit forms.
 */
class PrivateItemForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var PrivateItemInterface $item */
    $item = $this->entity;
    $itemTypeId = $item->bundle();
    $itemTypeStorage = $this->entityTypeManager->getStorage('private_item_type');
    $itemType = $itemTypeStorage->load($itemTypeId);

    $isNew = $item->isNew();
    $currentUser = $this->currentUser();

    if (!$isNew) {
      $form['#title'] = $this->t('Edit @type %label', [
        '@type' => $itemType->label(),
        '%label' => $item->label(),
      ]);
    }

    // Private item owner.
    $ownerId = $item->getOwnerId();
    $form['uid'] = [
      '#type' => 'value',
      '#value' => $ownerId ? $ownerId : $currentUser->id(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    $item = parent::buildEntity($form, $form_state);

    // Prevent leading and trailing spaces in private item names.
    $item->set('label', trim($item->label()));

    return $item;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $item = $this->entity;

    $result = $item->save();

    $edit_link = $item->toLink($this->t('Edit'), 'edit-form')->toString();
    switch ($result) {
      case SAVED_NEW:
        $this->logger('private_item')->notice('Created new private item %item.', array('%item' => $item->label(), 'link' => $edit_link));
        break;
      case SAVED_UPDATED:
        $this->logger('private_item')->notice('Updated private item %item.', array('%item' => $item->label(), 'link' => $edit_link));
        break;
    }

    // In the unlikely case something went wrong on save, the private item will
    // be rebuilt and private item form redisplayed.
    if (!$item->id()) {
      drupal_set_message(t('The item could not be saved.'), 'error');
      $form_state->setRebuild();
    }

    return $result;
  }

}
