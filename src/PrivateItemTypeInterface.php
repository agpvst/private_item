<?php

namespace Drupal\private_item;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a private item type entity.
 */
interface PrivateItemTypeInterface extends ConfigEntityInterface {

  /**
   * Returns the private item type description.
   *
   * @return string
   *   The private item type description.
   */
  public function getDescription();

}
