<?php

namespace Drupal\private_item\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\private_item\PrivateItemInterface;
use Drupal\private_item\PrivateItemTypeInterface;

/**
 * Provides route responses for private_item module.
 */
class PrivateItemController extends ControllerBase {

  /**
   * Returns a form to add a new private item.
   *
   * @param PrivateItemTypeInterface $private_item_type
   *   The private item type.
   *
   * @return array
   *   The private item add form.
   */
  public function addForm(PrivateItemTypeInterface $private_item_type) {
    $item = $this->entityTypeManager()->getStorage('private_item')->create([
      'type' => $private_item_type->id(),
    ]);
    return $this->entityFormBuilder()->getForm($item);
  }

  /**
   * The _title_callback for the private_item.add_form route.
   *
   * @param PrivateItemTypeInterface $private_item_type
   *   The current private item type.
   *
   * @return string
   *   The page title.
   */
  public function addFormTitle(PrivateItemTypeInterface $private_item_type) {
    return $this->t('Create @name', array('@name' => $private_item_type->label()));
  }

  /**
   * Route title callback.
   *
   * @param PrivateItemTypeInterface $private_item_type
   *   The private item type entity.
   *
   * @return array
   *   The private item type label as a render array.
   */
  public function itemTypeTitle(PrivateItemTypeInterface $private_item_type) {
    return ['#markup' => $private_item_type->label(), '#allowed_tags' => Xss::getHtmlTagList()];
  }

  /**
   * Route title callback.
   *
   * @param PrivateItemInterface $private_item
   *   The private item entity.
   *
   * @return array
   *   The private item label as a render array.
   */
  public function itemTitle(PrivateItemInterface $private_item) {
    return ['#markup' => $private_item->label(), '#allowed_tags' => Xss::getHtmlTagList()];
  }

}
